#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <ctype.h>
#include "info.h"


void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[1024];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2) {
         fprintf(stderr,"ERREUR, pas de port fourni\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERREUR ouverture socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERREUR appareillage");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERREUR avec acceptation");
    int e ;
    printf("Tous les tésultats ont été reçus \n \n"); 
    
    for (e = 0; e <=9; e++)
    {   
    ParcourTab(Tableau);
    int num1;
    num1 = Tableau[e];
    unsigned long long fact = 1;
    
    int i;
    
    if (num1 < 0)
        printf("Erreur! Le factoriel d'un negatif n'existe pas.");
    else {
        for (i = 1; i <= num1; ++i) {
            fact *= i;
        }
         printf("Factoriel de %d = %llu \n", num1, fact);
    }
}
    return 0;
}   

