package tp2v2;

import java.util.Scanner;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Server {
    
    public static void main(String[] args) throws Exception {
        
            //First : Serveur positionne sa socket d'ecoute sur le port 7777
            ServerSocket serverSocket= new ServerSocket(7777);

            //se met en attente de connexion de la part d'un client distant
            Socket socket = serverSocket.accept();
            
            //connexion accepté : récupére les flux objets pour communiquer
            //avec le client qui vient de se connecter
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
             
             //attente les données venant du client
             String chaine =(String)input.readObject();
             System.out.println("reçu :"+chaine);
             
             //affiche les coordonées du client qui vient de se conneceter 
             System.out.println("ça vient de :"+socket.getInetAddress()+" : "+socket.getPort());
             
             //envoienvoie d'une reponse au client
             
             output.writeObject(new String("bien reçu : "));
             
    }
    
}
