
package tp2v2;

//import java.util.Scanner;
//import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Client {
    
    public static void main(String[] args) throws Exception {
       System.out.println("hey");
             
             InetAddress adr = InetAddress.getByName("localhost");
             Socket socket = new Socket(adr,7777);
             
             //construction des flux objets à partir des flux de la socket
             ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
             
             //écriture d'une chaine dans le flux de sortie : càd ==> envoi de données au serveur
             output.writeObject(new String ("youpi"));
             
             //attente de réception de données venant du serveur (avec le readObject)
             //on sait qu'on attend une chaine, on peut donc faire un cast directement
             String chaine = (String)input.readObject();
             System.out.println("reçu de la part du serveur : "+chaine);
    }
    
    
    
}
