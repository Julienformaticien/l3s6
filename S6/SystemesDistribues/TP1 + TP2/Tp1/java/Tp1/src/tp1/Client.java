
package tp1;


 
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class Client {
    

  public static void main(String argv[]) throws Exception 
    { 
      DatagramSocket ds = new DatagramSocket();
      byte[] b = (new String("serveur t la ? ")).getBytes();
      InetAddress ia = InetAddress.getLocalHost();
      DatagramPacket dp = new DatagramPacket(b,b.length,ia,4999);
      ds.send(dp);
      String str = new String(dp.getData());
      System.out.println("** "+ str);
      //réponse
      ds.receive(dp);
      String chaine = new String(dp.getData(),0,dp.getLength());
      System.out.println("reçu de serveur : "+ chaine  );
                          

    } 
  
    
}