#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#define TAILLEBUF 20

int main() {

// adresse de la socket coté serveur
static struct sockaddr_in addr_client;

// adresse de la socket locale
static struct sockaddr_in addr_serveur;

//longueur adresse
int lg_addr;

//socket d'écoute et de service
int socket_ecoute,socket_service;

//buffer qui contiendra le message reçu
char message[TAILLEBUF];

//chaine reçue du clientTcp
char *chaine_recue;

//chaine renvoyée au clientTcp
char *reponse = "bien reçu";

//nombre d'octetc reçue ou envoyée
int nb_octets;

//  ** création socket TCP d'écoute **
socket_ecoute = socket(AF_INET,SOCK_STREAM,0);
if (socket_ecoute == -1) {
	perror("création socket");
	exit(1);
}

// liaison la socket sur le port local 4000
bzero((char *) &addr_serveur,sizeof(addr_serveur));
addr_serveur.sin_family = AF_INET;
addr_serveur.sin_port = htons(4000);
addr_serveur.sin_addr.s_addr=htonl(INADDR_ANY);
 if( bind(socket_ecoute, (struct sockaddr*)&addr_serveur, sizeof(addr_serveur))== -1 ) {
    perror("erreur bind socket ecoute");
   exit(1);
}

//configuration socket écoute : 5connexion max en attente
if (listen(socket_ecoute,5) == -1) {
  perror("erreur listen");
  exit(1);
}


// on attend la connexion du clientTcp
lg_addr = sizeof(struct sockaddr_in);
socket_service = accept(socket_ecoute,(struct sockaddr *)&addr_client,&lg_addr);
if (socket_service == -1) {
  perror("erreur accept");
  exit(1);
}


//la connexion est établie, on attend les données envoyées par le clientTcp
nb_octets = read (socket_service,message,TAILLEBUF);

//affichage du message reçu
chaine_recue = (char *)malloc(nb_octets * sizeof(char));
memcpy(chaine_recue,message,nb_octets);
printf("reçu message %s\n", chaine_recue);

//on envoie la reponse au clientTcp
write(socket_service,reponse,strlen(reponse)+1);

// fermeture la socket
close(socket_service);
close(socket_ecoute);
}
