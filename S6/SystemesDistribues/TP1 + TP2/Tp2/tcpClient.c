#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#define TAILLEBUF 20



int main() {

//adresse de la socket coté serveur
static struct sockaddr_in addr_serveur;

//identifiant de la machine serveur
struct hostent *serveur_host;

//descripteur de la socket locale
int sock;

//chaine à envoyer au serveurTcp
char *message = "bonjour";

//chaine ou sera écrit le message reçu
char reponse[TAILLEBUF];

//nombre d'octets envoyés/reçus
int nb_octets;

//  ** création d'une socket UDP **
sock = socket(AF_INET,SOCK_STREAM,0);
if (sock == -1) {
	perror("erreur création socket");
	exit(1);
}

//  ** récupération identifiant de la machine serveur **
serveur_host = gethostbyname("localhost");
if (serveur_host==NULL) {
	perror("erreur adresse serveur");
	exit(1);
}

//  ** création adresse socket destinatrice **
bzero((char *) &addr_serveur,sizeof(addr_serveur));
addr_serveur.sin_family = AF_INET;
addr_serveur.sin_port = htons(4000);
memcpy (&addr_serveur.sin_addr.s_addr,serveur_host -> h_addr, serveur_host -> h_length);

//coonexion de la socket client locale à la socket coté serveur
if (connect(sock,(struct sockaddr *)&addr_serveur,sizeof(struct sockaddr_in)) == -1 ) {
	perror("erreur connexion serveur");
	exit(1);
}

//connexion etablie, on envoie le message
nb_octets = write(sock,message,strlen(message)+1);

//on attend la réponse du serveur
nb_octets = read(sock,reponse,TAILLEBUF);
printf("reponse reçue :%s \n",reponse );

//on ferme la socket
close(sock);
}
