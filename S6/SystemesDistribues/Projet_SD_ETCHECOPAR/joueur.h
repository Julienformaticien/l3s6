#ifndef JOUEUR_H
#define JOUEUR_H

#include "common.h"


void initialisationFicheJoueur(struct FicheJoueur* maFiche,struct sockaddr_in* address_PortTCP,int socket_tcp);
int connexionMulticast(struct sockaddr_in* ad_sock_multicast);

#endif
