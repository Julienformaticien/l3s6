#ifndef COMMON_H
#define COMMON_H


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAX_JOUEURS 4

#define PSEUDO_UTILISE 101
#define JOUEUR_ACCEPTE 102

struct FicheJoueur
{
  int id;
  char pseudo[20];
  struct sockaddr_in addr_joueur;
};

int creationSocketTCP(int port);
void ajoutJoueur(struct FicheJoueur* tab_Joueur, int* nbJoueurs, struct FicheJoueur nouveau_joueur);
void supprimerJoueur(struct FicheJoueur* tab_Joueur, int* nbJoueurs, int idJoueur);

#endif
