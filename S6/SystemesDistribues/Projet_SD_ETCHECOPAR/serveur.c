#include "serveur.h"

/*
  SERVEUR
*/


/*
  Cree le groupe multicast
*/
void creationGroupeMulticast(int* fd, struct sockaddr_in* ad_multicast, struct ip_mreq* gr_multicast )
{
  *fd = socket(AF_INET, SOCK_DGRAM,0);
  //Reutilisation du port
  int reuse = 1;
  setsockopt(*fd, SOL_SOCKET, SO_REUSEADDR, (int *)& reuse, sizeof(reuse));

  memset((char *) ad_multicast, 0,sizeof(ad_multicast));

  ad_multicast->sin_family = AF_INET;
  ad_multicast->sin_addr.s_addr = htonl(INADDR_ANY);
  ad_multicast->sin_port = htons(PORT);

  bind(*fd,(struct sockaddr*) ad_multicast,sizeof(*ad_multicast));

  gr_multicast->imr_multiaddr.s_addr = inet_addr(GROUP);
  gr_multicast->imr_interface.s_addr = htonl(INADDR_ANY);

  setsockopt(*fd, IPPROTO_IP,IP_ADD_MEMBERSHIP, (char *) gr_multicast, sizeof(struct ip_mreq));
}


/*
  Compare un joueur avec la liste de joueur
*/
int comparerJoueur(struct FicheJoueur joueur, struct FicheJoueur* listeJoueurs,int nbJoueurs)
{
 int i = 0;
 while(i < nbJoueurs)
 {
   if(strcmp(joueur.pseudo,listeJoueurs[i].pseudo)==0)
    return PSEUDO_UTILISE;
   else i++;
 }
 return JOUEUR_ACCEPTE;
}

/*
  Initialisation de la liste de joueurs
*/
void initialisationListeJoueurs(struct FicheJoueur* tab_Joueur){
  struct FicheJoueur FicheVierge;
  FicheVierge.id=0;
  for (int i=0;i<MAX_JOUEURS;i++){
    memcpy(&tab_Joueur[i],&FicheVierge,sizeof(FicheVierge));
  }
}


/*
  Main
*/
int main(int argc, char *argv[]) {

  struct FicheJoueur listeJoueurs[MAX_JOUEURS];
  initialisationListeJoueurs(listeJoueurs);
  int nbJoueurs = 0;
  int fd;

  struct sockaddr_in ad_multicast;
  struct ip_mreq gr_multicast;
  creationGroupeMulticast(&fd, &ad_multicast, &gr_multicast);

  while(1)
  {
    if(nbJoueurs < MAX_JOUEURS)
    {
      struct FicheJoueur nouveau_joueur;
      int addrlen = sizeof(ad_multicast);
      int nb = recvfrom(fd,(struct FicheJoueur*)&nouveau_joueur,sizeof(nouveau_joueur),0,(struct sockaddr*)&ad_multicast,&addrlen);
      if(nb < 0)
      {
        perror("recvfrom");
        return 1;
      }
      switch(comparerJoueur(nouveau_joueur,listeJoueurs, nbJoueurs))
      {
        case PSEUDO_UTILISE :
          printf("Entrée d'un nouveau joueur REFUSEE\n");
          break;
        case JOUEUR_ACCEPTE :
          nouveau_joueur.id=nbJoueurs+1;
          ajoutJoueur(listeJoueurs, &nbJoueurs, nouveau_joueur);
          printf("Entrée d'un nouveau joueur :\n");
          printf("\tId : %d\n", listeJoueurs[nbJoueurs-1].id);
          printf("\tPseudo : %s\n", listeJoueurs[nbJoueurs-1].pseudo);

          int socket_service = creationSocketTCP(0);
          if (socket_service == -1)
          {
            perror("erreur creation socket_serviceServeur");
            exit(EXIT_FAILURE);
          }

          if (connect(socket_service, (struct sockaddr *) &nouveau_joueur.addr_joueur, sizeof(struct sockaddr_in)) == -1)
          {
            perror("erreur connexion socket_serviceServeur");
            exit(EXIT_FAILURE);
          }
          int nb_octets = write(socket_service, &listeJoueurs, sizeof(listeJoueurs));
          close(socket_service);

          break;
      }
      printf("\nListe des joueurs (%d) :\n", nbJoueurs);
      for (int j=0; j<nbJoueurs; j++){
        printf("Joueur %d : %s\n", j+1, listeJoueurs[j].pseudo);
      }
      printf("*******************************************\n\n");

    }
  }

  return 0;
}
