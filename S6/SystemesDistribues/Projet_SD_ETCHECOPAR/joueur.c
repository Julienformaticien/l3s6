#include "joueur.h"

#define PORT 1950
#define GROUP "239.255.255.255"

/*
  JOUEUR
*/

/*
  Rejoins le groupe multicast
*/
int connexionMulticast(struct sockaddr_in* ad_sock_multicast)
{
  int fd = socket(AF_INET,SOCK_DGRAM,0);
  if(fd < 0)
  {
    perror("socket");
    exit(EXIT_FAILURE);
  }

  memset(ad_sock_multicast,0,sizeof(ad_sock_multicast));

  ad_sock_multicast->sin_family = AF_INET;
  ad_sock_multicast->sin_addr.s_addr = inet_addr(GROUP);
  ad_sock_multicast->sin_port = htons(PORT);

  return fd;
}


/*
  Initialisation de la fiche du joueur local
*/
void initialisationFicheJoueur(struct FicheJoueur* maFiche,struct sockaddr_in* address_PortTCP,int socket_tcp)
{
  maFiche->id=0;

  char pseudo[20];
  char *positionEntree= NULL;
  printf("Entrez votre pseudo : ");
  if (fgets(pseudo, 20, stdin) != NULL)
  {
       positionEntree = strchr(pseudo, '\n');
       if (positionEntree != NULL)
       {
           *positionEntree = '\0';
       }
  }
  strcpy(maFiche->pseudo, pseudo);

  socklen_t foo = sizeof(address_PortTCP);
  if(getsockname(socket_tcp,(struct sockaddr*)&address_PortTCP,&foo)<0){
    perror("getsockname");
    exit(EXIT_FAILURE);
  }
  memcpy((struct sockaddr*)&maFiche->addr_joueur,(struct sockaddr*)&address_PortTCP,sizeof(address_PortTCP));
}


/*
  MAIN
*/
int main(int argc, char *argv[])
{
  struct sockaddr_in address_PortTCP, ad_sock_multicast, addr_client;
  struct ip_mreq gr_multicast;

  int fd;
  struct FicheJoueur maFiche;
  int nbJoueurs=0;
  struct FicheJoueur listeAutresJoueurs[MAX_JOUEURS];

  //creation d'une socket TCP serveur
  int socket_ecoute = creationSocketTCP(0);
  int socket_service= socket(AF_INET, SOCK_STREAM, 0);
  if (socket_service == -1)
  {
    perror("erreur creation socketClient");
    exit(EXIT_FAILURE);
  }

  //creation de la liste des sockets TCP clients
  int listeSocketsClients[MAX_JOUEURS-1];

  //Creation du groupe multicast
  fd=connexionMulticast(&ad_sock_multicast);

  initialisationFicheJoueur((struct FicheJoueur*)&maFiche,(struct sockaddr_in*)&address_PortTCP,socket_ecoute);

  //Envoi au groupe multicast
  int nb = sendto(fd,(struct FicheJoueur*)& maFiche,sizeof(maFiche),0,(struct sockaddr*)&ad_sock_multicast,sizeof(ad_sock_multicast));
  if(nb < 0)
  {
    perror("sendtoMulticast");
    return 1;
  }

  while(1)
  {
    if (listen(socket_ecoute,4)==-1)
    {
      perror("listenJoueur");
      close(socket_ecoute);
      exit(EXIT_FAILURE);
    }
    int lg_addr = sizeof(struct sockaddr_in);
    socket_service = accept(socket_ecoute, (struct sockaddr *)&addr_client, &lg_addr);
    if(socket_service == -1)
    {
      perror("acceptJoueur");
      exit(1);
    }

    int nb_octets = read(socket_service, listeAutresJoueurs, sizeof(listeAutresJoueurs));
    int j=0;

    printf("\nVous êtes inscrits à la prochaine partie !\n");
    printf("\nListe des joueurs :\n");
    while ((j<MAX_JOUEURS) && (listeAutresJoueurs[j].id!=0)){
      if(strcmp(listeAutresJoueurs[j].pseudo, maFiche.pseudo)==0){
        printf("Moi : %s, (id : %d)\n", listeAutresJoueurs[j].pseudo, listeAutresJoueurs[j].id);
        maFiche.id=listeAutresJoueurs[j].id;
        supprimerJoueur(listeAutresJoueurs,&nbJoueurs,maFiche.id);
      } else {
        printf("Joueur %d : %s\n", j+1, listeAutresJoueurs[j].pseudo);
      }
      j++;
    }
    nbJoueurs=j;
    printf("*******************************************\n\n");
  }

  return 0;
}
