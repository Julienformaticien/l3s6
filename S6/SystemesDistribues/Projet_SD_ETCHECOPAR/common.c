#include "common.h"


/*
  Cree une socket TCP
*/
int creationSocketTCP(int port)
{
  int socket_ecoute;
  socket_ecoute = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_ecoute == -1)
  {
    perror("erreur creation socket creationSocketTCP");
    exit(1);
  }
  struct sockaddr_in addr_serveur;
  bzero((char *) &addr_serveur, sizeof(addr_serveur));
  addr_serveur.sin_family = AF_INET;
  addr_serveur.sin_port = htons(port);
  addr_serveur.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(socket_ecoute, (struct sockaddr*)&addr_serveur, sizeof(addr_serveur)) == -1)
  {
    perror("erreur bind creationSocketTCP");
    close(socket_ecoute);
    exit(EXIT_FAILURE);
  }
  return socket_ecoute;
}



/*
  Ajoute un joueur à la liste de joueur
*/
void ajoutJoueur(struct FicheJoueur* tab_Joueur, int* nbJoueurs, struct FicheJoueur nouveau_joueur)
{
  memcpy(&tab_Joueur[*nbJoueurs],&nouveau_joueur,sizeof(nouveau_joueur));
  *nbJoueurs=*nbJoueurs+1;
}

/*
  Supprime un joueur de la liste de joueur
*/
void supprimerJoueur(struct FicheJoueur* tab_Joueur, int* nbJoueurs, int idJoueur){
  int i = 0;
  while((i < *nbJoueurs)||(tab_Joueur[i].id != idJoueur))
  {
    i++;
  }
  if (i != *nbJoueurs){
    struct FicheJoueur FicheVierge;
    FicheVierge.id=0;
    for (i; i<*nbJoueurs; i++){
      if (i == (*nbJoueurs)-1)
        memcpy(&tab_Joueur[i],&FicheVierge,sizeof(FicheVierge));
      else
        memcpy(&tab_Joueur[i],&tab_Joueur[i+1],sizeof(tab_Joueur[i+1]));
    }
    *nbJoueurs--;
  }
}
