#ifndef SERVEUR_H
#define SERVEUR_H


#include "common.h"

#define PORT 1950
#define GROUP "239.255.255.255"

int compare(struct FicheJoueur joueur, struct FicheJoueur* tab_info,int nb_joueur);
void creationGroupeMulticast(int* fd, struct sockaddr_in* ad_multicast, struct ip_mreq* gr_multicast );
#endif
