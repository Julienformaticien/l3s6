with TEXT_IO; use TEXT_IO; 

--sauvegarder ce fichier sous le nom de: Cheminots2.adb, et pour compiler lancer: gnatmake Cheminots2.adb -o Cheminots2

procedure Cheminots2 is
package int_io is new Integer_io(integer);
use int_io;

PierreDansPanier:boolean:=false;

task peruvien;
task body peruvien is

I:integer;


begin

--pour tester je prends 3 iterations par exemple et non pas une boucle infinie

for I in 1..3 loop
-- protocole d entree
        while (not (PierreDansPanier)) loop
                        Put_Line("PERUVIEN fait la SIESTE");
                end loop;


-- process1 entre en SC, PEROU est sur AB
Put_Line ("PERUVIEN passe le col, section critique");

-- protocole de sortie
Put_Line ("PERUVIEN arrête son train et retourne enlever la pierre");
PierreDansPanier:=false;

end loop;
end peruvien;


task bolivien;
task body bolivien is

J:integer;

Begin
--pour tester je prends 3 iterations et non pas une boucle infinie

for J in 1..3 loop
-- protocole d entree
        while (PierreDansPanier) loop
                        Put_Line ("BOLIVIEN fait la SIESTE");
        end loop;


-- process2 entre en SC, BOLIVIE sur AB
Put_Line ("BOLIVIEN passe le col, section critique");

-- protocole de sortie
Put_Line ("BOLIVIEN arrête son train et retourne mettre une pierre dans le panier");
PierreDansPanier:=true;

end loop;
end bolivien;


begin
Null;
end Cheminots2;
