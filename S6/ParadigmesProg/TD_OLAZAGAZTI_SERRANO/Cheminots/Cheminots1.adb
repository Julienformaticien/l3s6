with TEXT_IO; use TEXT_IO;

--sauvegarder ce fichier sous le nom de: Cheminots1.adb, et pour compiler lancer: gnatmake Cheminots1.adb -o Cheminots1

procedure Cheminots1 is
package int_io is new Integer_io(integer);
use int_io;

PierreDansPanier:boolean:=false;
CollisionPossible:boolean:=false;

task processus1;
task body processus1 is

I:integer;


begin

--pour tester je prends 3 iterations par exemple et non pas une boucle infinie

for I in 1..3 loop
-- protocole d entree
        Put_Line ("PERUVIEN arrête son train");
        while (PierreDansPanier) loop
                        Put_Line("PERUVIEN fait la SIESTE");
                end loop;


Put_Line ("PERUVIEN cherche une pierre");
Put_Line ("PERUVIEN met la pierre dans le panier");
PierreDansPanier:=true;
Put_Line ("PERUVIEN passe le col");

-- process1 entre en SC, PEROU est sur AB

if(CollisionPossible) then
      Put_Line("---------------------------------------------------------------------COLLISION COLLISION COLLISION COLLISION");
end if;
CollisionPossible:=true;
Put_Line ("PERUVIEN est sur AB, section critique");
CollisionPossible:=false;

-- protocole de sortie
Put_Line ("PERUVIEN arrête son train, retourne au panier à pied et enlève sa pierre");
PierreDansPanier:=false;
Put_Line ("PERUVIEN redémarre");

end loop;
end processus1;


task processus2;
task body processus2 is

J:integer;

Begin
--pour tester je prends 3 iterations et non pas une boucle infinie

for J in 1..3 loop
-- protocole d entree
        Put_Line ("BOLIVIEN arrete son train");
        while (PierreDansPanier) loop
                        Put_Line ("BOLIVIEN fait la SIESTE");
        end loop;

Put_Line ("BOLIVIEN cherche une pierre");
Put_Line ("BOLIVIEN met la pierre dans le panier");
Put_Line ("BOLIVIEN passe le col");
PierreDansPanier:=true;


if(CollisionPossible) then
      Put_Line("---------------------------------------------------------------------COLLISION COLLISION COLLISION COLLISION");
end if;
CollisionPossible:=true;
-- process2 entre en SC, BOLIVIE sur AB
Put_Line ("BOLIVIEN est sur AB, section critique");
CollisionPossible:=false;

-- protocole de sortie
Put_Line ("BOLIVIEN arrête son train, retourne au panier à pied et enlève sa pierre");
PierreDansPanier:=false;
Put_Line ("BOLIVIEN redémarre");

end loop;
end processus2;


begin
Null;
end Cheminots1;
