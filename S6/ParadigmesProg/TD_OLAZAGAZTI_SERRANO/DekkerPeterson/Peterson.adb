with TEXT_IO; use TEXT_IO;

--sauvegarder ce fichier sous le nom de: Peterson.adb, et pour compiler lancer: gnatmake Peterson.adb -o Peterson

procedure Peterson is
package int_io is new Integer_io(integer);
use int_io;

demande1:boolean:=false;
demande2:boolean:=false;
tour:integer;

task processus1;
task body processus1 is

I:integer;


begin

--pour tester je prends 2 iterations par exemple et non pas une boucle infinie

for I in 1..2 loop
-- protocole d entree
        demande1:=true;
        tour:=2;
        while (demande2 and (tour/=1)) loop
                        Put_Line("processus1 en attente");
                end loop;


-- process1 entre en SC
  Put_Line ("processus1 en SC");

-- protocole de sortie
  demande1:=false;

end loop;
end processus1;


task processus2;
task body processus2 is

J:integer;

Begin
--pour tester je prend 2 iterations et non pas une boucle infinie

for J in 1..2 loop
-- protocole d entree
        demande2:=true;
        tour:=1;
        while (demande1 and (tour/=2)) loop
                        Put_Line ("processus2 en attente");
        end loop;


-- process2 entre en SC
  Put_Line ("processus2 en SC");

-- protocole de sortie
  demande2:=false;

end loop;
end processus2;


begin
Null;
end Peterson;
