with TEXT_IO; use TEXT_IO;

--sauvegarder ce fichier sous le nom de: PetersonSymetrique.adb, et pour compiler lancer: gnatmake PetersonSymetrique.adb -o PetersonSymetrique

procedure PetersonSymetrique is
package int_io is new Integer_io(integer);
use int_io;

demande: array(0..1) of boolean := (false, false);
tour:integer;

-- PROCEDURE : protocole d'entree
procedure protocoleEntree(numProcessus: in integer) is

begin

  demande(numProcessus):=true;
  tour:=(numProcessus+1) mod 2;
  while ((tour/=numProcessus) and (demande((numProcessus+1) mod 2)=true) ) loop
                  Put_Line("processus en attente");
          end loop;

end protocoleEntree;


-- PROCEDURE : protocole de sortie
procedure protocoleSortie(numProcessus: in integer) is

begin

  demande(numProcessus):=false;

end protocoleSortie;




task processus1;
task body processus1 is

I:integer;


begin

--pour tester je prends 2 iterations par exemple et non pas une boucle infinie

for I in 1..2 loop
-- protocole d entree
  protocoleEntree(0);

-- process1 entre en SC
  Put_Line ("processus1 en SC");

-- protocole de sortie
  protocoleSortie(0);

end loop;
end processus1;


task processus2;
task body processus2 is

J:integer;

Begin
--pour tester je prend 2 iterations et non pas une boucle infinie

for J in 1..2 loop

  -- protocole d entree
    protocoleEntree(1);

  -- process1 entre en SC
    Put_Line ("processus2 en SC");

  -- protocole de sortie
    protocoleSortie(1);

end loop;
end processus2;


begin
Null;
end PetersonSymetrique;
