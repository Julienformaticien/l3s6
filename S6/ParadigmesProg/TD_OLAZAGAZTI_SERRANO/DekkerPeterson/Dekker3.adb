with TEXT_IO; use TEXT_IO;

--sauvegarder ce fichier sous le nom de: Dekker3.adb, et pour compiler lancer: gnatmake Dekker3.adb -o Dekker3

procedure Dekker3 is
package int_io is new Integer_io(integer);
use int_io;

P1WantsToEnter:boolean:=false;
P2WantsToEnter:boolean:=false;

task processus1;
task body processus1 is

I:integer;


begin

--pour tester je prends 2 iterations par exemple et non pas une boucle infinie

for I in 1..2 loop
-- protocole d entree
        P1WantsToEnter:=true;
        while (P2WantsToEnter) loop
                        Put_Line("processus1 en attente");
                end loop;


-- process1 entre en SC
  Put_Line ("processus1 en SC");

-- protocole de sortie
  P1WantsToEnter:=false;

end loop;
end processus1;


task processus2;
task body processus2 is

J:integer;

Begin
--pour tester je prend 2 iterations et non pas une boucle infinie

for J in 1..2 loop
-- protocole d entree
        P2WantsToEnter:=true;
        while (P1WantsToEnter) loop
                        Put_Line ("processus2 en attente");
        end loop;


-- process2 entre en SC
  Put_Line ("processus2 en SC");

-- protocole de sortie
  P2WantsToEnter:=false;

end loop;
end processus2;


begin
Null;
end Dekker3;
