with TEXT_IO; use TEXT_IO;

--sauvegarder ce fichier sous le nom de: TroisProcessusSymetrique.adb, et pour compiler lancer: gnatmake TroisProcessusSymetrique.adb -o TroisProcessusSymetrique

procedure TroisProcessusSymetrique is
package int_io is new Integer_io(integer);
use int_io;

demande: array(0..2) of boolean := (false, false, false);
tour:integer;

-- PROCEDURE : protocole d'entree
procedure protocoleEntree(numProcessus: in integer) is

doitAttendre:boolean := true;

begin

    demande(numProcessus):=true;
    if (demande((numProcessus+1) mod 3) = true) then
        tour:=(numProcessus+1) mod 3;
    else if (demande((numProcessus+2) mod 3) = true) then
        tour:=(numProcessus+2) mod 3;
      end if;
    end if;
    while (doitAttendre) loop
        if((tour /= numProcessus) and (demande((numProcessus+1) mod 3) or demande((numProcessus+2) mod 3))) then
          Put_Line ("processus en attente");
        else
          doitAttendre:=false;
        end if;
    end loop;

end protocoleEntree;


-- PROCEDURE : protocole de sortie
procedure protocoleSortie(numProcessus: in integer) is

begin

    demande(numProcessus):=false;
    tour:=(numProcessus+1) mod 3;

end protocoleSortie;




task processus1;
task body processus1 is

I:integer;


begin

--pour tester je prends 2 iterations par exemple et non pas une boucle infinie

for I in 1..2 loop
-- protocole d entree
  protocoleEntree(0);

-- process1 entre en SC
  Put_Line ("processus1 en SC");

-- protocole de sortie
  protocoleSortie(0);

end loop;
end processus1;


task processus2;
task body processus2 is

J:integer;

Begin
--pour tester je prend 2 iterations et non pas une boucle infinie

for J in 1..2 loop

  -- protocole d entree
    protocoleEntree(1);

  -- process1 entre en SC
    Put_Line ("processus2 en SC");

  -- protocole de sortie
    protocoleSortie(1);

end loop;
end processus2;

task processus3;
task body processus3 is

K:integer;

Begin
--pour tester je prend 2 iterations et non pas une boucle infinie

for K in 1..2 loop

  -- protocole d entree
    protocoleEntree(2);

  -- process1 entre en SC
    Put_Line ("processus3 en SC");

  -- protocole de sortie
    protocoleSortie(2);

end loop;
end processus3;


begin
Null;
end TroisProcessusSymetrique;
