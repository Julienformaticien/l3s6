#include "Algo_Ford_Fulkerson.h"

class Fulkerson{
  public:
    static bool algo(int graph[TAILLE][TAILLE], int , int){
      int Graphe_Appliq[TAILLE][TAILLE];
      bool Passage[TAILLE];

      for (int i = 0; i < V; i++)
          for (int j = 0; j < TAILLE; j++)
               Graphe_Appliq[i][j] = graphe[i][j];
      int Parent[TAILLE];
      int Flot_Max_Graphe (0);
      do{
          int Chemin_Flot_Graphe = 2147483647;
          j = fin;
          while(j != debut){
            i = Parent[js];
            Chemin_Flot_Graphe = min(Chemin_Flot_Graphe, Graphe_Appliq[i][j]);
            j=Parent[j];
          }
          j = fin;
          while(j != debut){
            i = parent[j];
            Graphe_Appliq[i][j] -= Chemin_Flot_Graphe;
            Graphe_Appliq[j][i] += Chemin_Flot_Graphe;
            j=Parent[j];
          }

          Flot_Max_Graphe += Chemin_Flot_Graphe;
      }while (Fulkerson::check(Graphe_Appliq, debut, fin, Parent))
      return Flot_Max_Graphe;
    }
    static bool check(int Graphe_Appliq[TAILLE][TAILLE], int , int , int[]){

          bool Passage[TAILLE];
          for(int i = 0; i < sizeof(Passage); i++){
            Passage[i] = false;
          }
          std::queue <int> Queue_Graphe;
          Queue_Graphe.push(debut);
          Passage[debut] = true;
          Parent[debut] = -1;
          do
          {
              int i = Queue_Graphe.front();
              Queue_Graphe.pop();
              for (int j = 0; j < TAILLE; j++)
              {
                  if (Graphe_Appliq[i][j] > 0 && Passage[j]==false)
                  {
                      Queue_Graphe.push(j);
                      Parent[j] = i;
                      Passage[j] = true;
                  }
              }
          }while(!Queue_Graphe.empty())
          return (Passage[fin]);
    }
}
int main()
{
    int Graphe_Trader[TAILLE][TAILLE] = { {0, 100, 100, 0, 0, 0, 0, 0, 0}, //Représentation de la source
                        {0, 0, 0, 60, 0, 30, 0, 0, 0}, //Représentation de E1
                        {0, 0, 0, 50, 40, 0, 0, 0, 0}, //Représentation de E2
                        {0, 0, 0, 0, 20, 30, 50, 40, 0}, //Représentation de F1
                        {0, 0, 0, 0, 0, 0, 0, 50, 0}, //Représentation de F2
                        {0, 0, 0, 0, 0, 0, 0, 0, 60}, //Représentation de A1
                        {0, 0, 0, 0, 0, 0, 0, 0, 50}, //Représentation de A2
                        {0, 0, 0, 0, 0, 0, 0, 0, 90}, //Représentation de A3
                        {0, 0, 0, 0, 0, 0, 0, 0, 0} //Représentation du puit
                      };
    int flMax(Fulkerson::algo(Graphe_Trader, 0, 8));
    std::cout << "Après avoir appliqué l'algorithme de Ford-Fulkerson, le flot max est de : " << flmax <<"\n";
    return 0;
}
